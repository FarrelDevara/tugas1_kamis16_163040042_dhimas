<?php
	class Jenis_model{
		private $table = 'jenis';
		private $db;
		
		public function __construct(){
			$this->db = new Database;
		}
							
		public function getAllObat($tabel){
			$this->table = $tabel;
			$this->db->query('SELECT * FROM '. $this->table);
			return $this->db->resultSet();
		}
		
		public function getElementById($table,$id){
			$this->table = $table;
			$this->db->query("SELECT * FROM ". $this->table ." WHERE id_jenis=:id");
			$this->db->bind("id",$id);
			return $this->db->single();
		}
		
		public function insert($tabel, $data = []){
			$this->table = $tabel;
			$this->db->query("insert into ". $this->table ." (nama_jenis)
			values(:nama)");
			$this->db->bind("nama", $data["nama"]);
			$this->db->execute();
		}
		
		public function ubah($tabel, $data = []){
			$this->table = $tabel;
			$this->db->query("update ". $this->table ." set nama_jenis=:nama where id_jenis=:id");
			$this->db->bind("nama", $data["nama"]);
			$this->db->bind("id", $data["id"]);
			$this->db->execute();
		}
		
		public function hapus($tabel, $id){
			$this->table = $tabel;
			$this->db->query("delete from ". $this->table .' where id_jenis=:id');
			$this->db->bind("id",$id);
			$this->db->execute();
		}
		
		public function cari($tabel, $data){
			$this->table = $tabel;
			$this->db->query("SELECT * FROM ". $this->table ." WHERE id_jenis=:cari OR nama_jenis=:cari");
			$this->db->bind("cari",$data);
			return $this->db->resultSet();
		}
	}